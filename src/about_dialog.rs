use crate::config::{APP_ID, VERSION};
use gtk4::{prelude::*, License, Widget};
use libadwaita::prelude::*;
use libadwaita::AboutDialog;

pub const APP_NAME: &str = "Newsflash";
pub const COPYRIGHT: &str = "Copyright © 2017-2023 Jan Lukas Gernert";
pub const DESCRIPTION: &str = r#"<b>Follow your favorite blogs &amp; news sites.</b>

Newsflash is an application designed to complement an already existing web-based RSS reader account.

It combines all the advantages of web based services like syncing across all your devices with everything you expect from a modern desktop application.

<b>Features:</b>

 - Desktop notifications
 - Fast search and filtering
 - Tagging
 - Keyboard shortcuts
 - Offline support
 - and much more

<b>Supported Services:</b>

 - Miniflux
 - local RSS
 - fever
 - Fresh RSS
 - NewsBlur
 - Inoreader
 - feedbin
 - Nextcloud News"#;
pub const WEBSITE: &str = "https://gitlab.com/news_flash/news_flash_gtk";
pub const ISSUE_TRACKER: &str = "https://gitlab.com/news_flash/news_flash_gtk/-/issues";
pub const AUTHORS: &[&str] = &["Jan Lukas Gernert", "Felix Bühler", "Alistair Francis"];
pub const DESIGNERS: &[&str] = &["Jan Lukas Gernert"];
pub const ARTISTS: &[&str] = &["Tobias Bernard"];

pub const RELEASE_NOTES: &str = r#"
<ul>
    <li>article scraper: add null checks in hope to prevent crashes</li>
    <li>FreshRSS: automatically add missing slash to Url on login</li>
    <li>limit concurrent HTTP calls to avoid DNS issues</li>
    <li>stop using trust DNS</li>
    <li>add share to clipboard</li>
    <li>fix mark all as read when closing the app right afterwards</li>
    <li>revert user agent change of 3.1.0</li>
</ul>
"#;

#[derive(Clone, Debug)]
pub struct NewsFlashAbout;

impl NewsFlashAbout {
    pub fn show<W: IsA<Widget> + WidgetExt>(window: &W) {
        AboutDialog::builder()
            .application_name(APP_NAME)
            .application_icon(APP_ID)
            .developer_name(AUTHORS[0])
            .developers(AUTHORS)
            .designers(DESIGNERS)
            .artists(ARTISTS)
            .license_type(License::Gpl30)
            .version(VERSION)
            .website(WEBSITE)
            .issue_url(ISSUE_TRACKER)
            .comments(DESCRIPTION)
            .copyright(COPYRIGHT)
            .release_notes(RELEASE_NOTES)
            .build()
            .present(window)
    }
}
