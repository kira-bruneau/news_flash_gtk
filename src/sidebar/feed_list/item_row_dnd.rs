use glib::subclass;
use gtk4::prelude::*;
use gtk4::{subclass::prelude::*, Box, CompositeTemplate, Image, Label, Widget};
use log::warn;
use news_flash::error::NewsFlashError;
use std::str;

use crate::{app::App, sidebar::FeedListItemID};

mod imp {
    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(file = "data/resources/ui_templates/sidebar/feedlist_dnd_icon.blp")]
    #[derive(Default)]
    pub struct ItemRowDnD {
        #[template_child]
        pub item_count_label: TemplateChild<Label>,
        #[template_child]
        pub title: TemplateChild<Label>,
        #[template_child]
        pub favicon: TemplateChild<Image>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ItemRowDnD {
        const NAME: &'static str = "ItemRowDnD";
        type ParentType = Box;
        type Type = super::ItemRowDnD;

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ItemRowDnD {}

    impl WidgetImpl for ItemRowDnD {}

    impl BoxImpl for ItemRowDnD {}
}

glib::wrapper! {
    pub struct ItemRowDnD(ObjectSubclass<imp::ItemRowDnD>)
        @extends Widget, Box;
}

impl Default for ItemRowDnD {
    fn default() -> Self {
        glib::Object::new::<Self>()
    }
}

impl ItemRowDnD {
    pub fn new(id: FeedListItemID, title: &str, count: u32) -> Self {
        let widget = Self::default();
        widget.update_favicon(Some(id));
        widget.update_title(title);
        widget.update_item_count(count);
        widget
    }

    fn update_title(&self, title: &str) {
        let imp = self.imp();
        imp.title.set_text(title);
    }

    fn update_item_count(&self, count: u32) {
        let imp = self.imp();

        if count > 0 {
            imp.item_count_label.set_label(&count.to_string());
            imp.item_count_label.set_visible(true);
        } else {
            imp.item_count_label.set_visible(false);
        }
    }

    fn update_favicon(&self, feed_id: Option<FeedListItemID>) {
        let imp = self.imp();
        let favicon = self.imp().favicon.clone();

        if let Some(FeedListItemID::Feed(feed_mapping)) = feed_id {
            App::default().execute_with_callback(
                move |news_flash, client| async move {
                    let res = if let Some(news_flash) = news_flash.read().await.as_ref() {
                        news_flash.get_icon(&feed_mapping.feed_id, &client).await
                    } else {
                        Err(NewsFlashError::NotLoggedIn)
                    };

                    if let Err(error) = res.as_ref() {
                        warn!("Failed to load favicon for feed: '{}': {error}", feed_mapping.feed_id);
                    }

                    res
                },
                move |_app, res| {
                    let icon = match res {
                        Err(_) => return,
                        Ok(icon) => icon,
                    };

                    if let Some(data) = icon.data {
                        let bytes = glib::Bytes::from_owned(data);
                        let texture = gdk4::Texture::from_bytes(&bytes);
                        if let Ok(texture) = texture {
                            favicon.set_visible(true);
                            favicon.set_from_paintable(Some(&texture));
                        } else if let Err(error) = texture {
                            log::debug!("Favicon '{}': {}", icon.feed_id, error);
                        }
                    }
                },
            );
        } else {
            imp.favicon.set_visible(false);
        }
    }
}
